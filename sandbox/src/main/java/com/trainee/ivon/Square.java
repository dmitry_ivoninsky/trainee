package com.trainee.ivon;

public class Square {
    public int l;

    public Square(int l) {
        this.l = l;
    }

    public int area() {

        return this.l * this.l;
    }
}