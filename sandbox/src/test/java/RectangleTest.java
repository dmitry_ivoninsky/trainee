import com.trainee.ivon.Rectangle;
import org.testng.annotations.Test;

public class RectangleTest {
    @Test
    public void testArea(){
        Rectangle r = new Rectangle(5, 6 );
        assert r.area() == 30;
    }
}
